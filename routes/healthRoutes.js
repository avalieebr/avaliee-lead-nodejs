const express = require('express');
const router = express.Router();

const healthController = require('./../controllers/healthController');

router
    .route('/liveness')
    .get(healthController.liveness);

router
    .route('/readiness')
    .get(healthController.readiness);

module.exports = router;

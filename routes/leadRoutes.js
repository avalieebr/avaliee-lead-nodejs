const express = require('express');
const leadController = require('./../controllers/leadController');
const auth = require('./../auth');

const requireAdmin = auth.hasRole('avaliee_admin');

const router = express.Router();

router
    .route('/')
    .post(leadController.createLead)
    .get(requireAdmin, leadController.getAllLeads);

router
    .route('/:id')
    .get(requireAdmin, leadController.getLeadById)
    .delete(requireAdmin, leadController.deleteLeadById);

module.exports = router;

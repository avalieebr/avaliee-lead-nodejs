const publicRouter = require('./publicRoutes');
const healthRouter = require('./healthRoutes');
const leadRouter = require('./leadRoutes');

module.exports = app => {
    app.use('/', publicRouter);
    app.use('/health', healthRouter);
    app.use('/api/v1/leads', leadRouter);
};

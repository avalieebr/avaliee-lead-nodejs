const Keycloak = require('keycloak-connect');

const keycloak = new Keycloak({});

module.exports = {
    jwt: () => keycloak.middleware(),
    hasRole: (role) => keycloak.protect('realm:' + role)
};

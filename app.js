const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

const auth = require('./auth');
const routes = require('./routes');

const app = express();

if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

app.use(auth.jwt());
app.use(bodyParser.json());
app.use(cors());

routes(app);

module.exports = app;

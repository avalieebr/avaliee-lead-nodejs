const models = require('../db/models');

exports.createLead = async (req, res) => {
    try {
        const newLead = await models.Lead.create(req.body);

        res.status(201)
            .location('/api/v1/leads/' + newLead.id)
            .json(newLead);
    } catch (err) {
        res.status(500).send(err);
    }
};

exports.getAllLeads = async (req, res) => {
    try {
        const leads = await models.Lead.findAll({ where: { deleted: false } });

        res.status(200)
            .json(leads);
    } catch (err) {
        res.status(500).send(err);
    }
};

exports.getLeadById = async (req, res) => {
    try {
        const lead = await models.Lead.findOne({ where: { id: req.params.id, deleted: false } });

        if (lead === null) {
            res.status(404).send();
        } else {
            res.status(200)
                .json(lead);
        }
    } catch (err) {
        res.status(500).send(err);
    }
};

exports.deleteLeadById = async (req, res) => {
    try {
        const deleted = await models.Lead.update(
            { deleted: true },
            { where: { id: req.params.id, deleted: false } }
        );

        if (deleted[0] == 0) {
            res.status(404).send();
        } else {
            res.status(204).send();
        }
    } catch (err) {
        console.log(err);
        res.status(500).send(err);
    }
};

exports.liveness = (req, res) => {
    res
        .status(200)
        .json({status: 'UP'});
};

exports.readiness = (req, res) => {
    res
        .status(200)
        .json({status: 'READY'});
};

require('dotenv').config()

module.exports = {
  development: {
    host: process.env.DATABASE_URL,
    database:'avaliee_lead',
    dialect: 'postgres',
    username: 'postgres',
    password: 'postgres',
  },
  test: {
    host: process.env.DATABASE_URL,
    database:'avaliee_lead',
    dialect: 'postgres',
    username: 'postgres',
    password: 'postgres',
  },
  production: {
    host: process.env.DATABASE_URL,
    database:'avaliee_lead',
    dialect: 'postgres',
    username: 'postgres',
    password: 'postgres',
  },
}
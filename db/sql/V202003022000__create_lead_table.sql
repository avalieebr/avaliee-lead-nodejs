CREATE TABLE IF NOT EXISTS public.lead 
(
  id               UUID NOT NULL DEFAULT uuid_generate_v4 (),
  created_at       TIMESTAMP NOT NULL,
  updated_at       TIMESTAMP NOT NULL,
  customer_name    VARCHAR(255) NOT NULL,
  customer_email   VARCHAR(255),
  customer_phone   VARCHAR(255),
  search_result    JSONB,
  deleted          BOOLEAN NOT NULL DEFAULT FALSE,
  PRIMARY KEY (id)
);
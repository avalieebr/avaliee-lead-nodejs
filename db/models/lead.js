'use strict';

const Sequelize = require('sequelize');
const DataTypes = require('sequelize/lib/data-types');

module.exports = function (sequelize) {

  /**
   * The Lead table defines the lead itself.
   */
  const Lead = sequelize.define('Lead', {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      primaryKey: true,
      allowNull: false,
    },
    customerName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    customerEmail: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    customerPhone: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    searchResult: {
      type: Sequelize.JSONB,
      allowNull: false,
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
  });

  return Lead;
};

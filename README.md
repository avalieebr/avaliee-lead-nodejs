# Avaliee Lead

Application exposing a HTTP API for capturing and managing *leads* created on the _Avaliee_ webapp.

## Development

This is `Node.js` application built with:

- `Node.js v12.16.1+`
- `npm v6.13.4`

#### Stack

The application uses and integrates with:

- `PostgreSQL v9.6.17`
- `Keycloak v9.0.0`

### Setup

- Clone this repository
```bash
$ git clone git@bitbucket.org:avalieebr/avaliee-lead.git
```

- Install dependencies
```bash
$ cd avaliee-lead
$ npm install
```

#### Environment

The application stack can be set up via [`Docker`](https://docs.docker.com/) containers by running the following comand in the project root directory:

- Create a common network
```bash
$ docker network create avaliee-network
```

- Run a PostgreSQL container
```bash
$ docker run --name postgres9.6 --net avaliee-network -p 5432:5432 -v $(pwd)/.postgres/init-dbs.sh:/docker-entrypoint-initdb.d/init-dbs.sh -e POSTGRES_PASSWORD=postgres -d postgres:9.6
```

- Run a Keycloak container
```bash
$ docker run --name keycloak9 --net avaliee-network -p 8080:8080 -v $(pwd)/.keycloak/avaliee.json:/tmp/avaliee.json -e DB_VENDOR=postgres -e DB_USER=postgres -e DB_PASSWORD=postgres -e DB_ADDR=postgres9.6 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e KEYCLOAK_IMPORT=/tmp/avaliee.json jboss/keycloak:9.0.0
```

Or via [`docker-compose`](https://docs.docker.com/compose/) by just running:
```bash
$ docker-compose up
```

## Running

### Development mode
```bash
$ npm run dev
```

### Production mode
```bash
$ npm run start
```

### Container

- Build the `Docker` image:
```bash
$ docker build -t avaliee/avaliee-lead .
```

- Run a container:
```bash
$ docker run --init -it --rm --net host -e NODE_ENV=production avaliee/avaliee-lead
```

## Architecture overview

![](./docs/arch-overview.png)

## API

The endpoints to operate on the aplication features are defined in the *leads* resource.

#### Creating a lead
```bash
POST /api/v1/leads

Content-Type: application/json
{
  "customerName": String,
  "customerEmail": String,
  "customerPhone": String,
  "searchResult": Object
}
```

#### Listing leads
```bash
GET /api/v1/leads

Authorization: Bearer access_token
```

#### Fetching a specific lead
```bash
GET /api/v1/leads/:leadId

Authorization: Bearer access_token
```

#### Deleting a specific lead
```bash
DELETE /api/v1/leads/:leadId

Authorization: Bearer access_token
```

### Usage example

With the stack and the application up and running, tests can be performed using, e.g, [`curl`](https://curl.haxx.se/) and [`jq`](https://stedolan.github.io/jq/) on a terminal.

#### Creating a lead
```bash
$ curl -v -H "Content-Type: application/json" http://localhost:8000/api/v1/leads -d '{"customerName": "Foo Bar", "customerEmail": "foo@bar.email", "customerPhone": "199901876303", "searchResult": {}}' | jq
```
Expected response:
```
< HTTP/1.1 201 Created
< X-Powered-By: Express
< Location: /api/v1/leads/740b6777-bc0d-4ee3-9032-694d792a91b9
< Content-Type: application/json; charset=utf-8
< Content-Length: 245
< ETag: W/"f5-RfPlxZjvpnXoz8L+nzuvmevUIkw"
< Date: Thu, 05 Mar 2020 02:06:02 GMT
< Connection: keep-alive
< 
{ [245 bytes data]
100   359  100   245  100   114  17500   8142 --:--:-- --:--:-- --:--:-- 25642
* Connection #0 to host localhost left intact
{
  "id": "740b6777-bc0d-4ee3-9032-694d792a91b9",
  "deleted": false,
  "customerName": "Foo Bar",
  "customerEmail": "foo@bar.email",
  "customerPhone": "199901876303",
  "searchResult": {},
  "updatedAt": "2020-03-05T02:06:02.741Z",
  "createdAt": "2020-03-05T02:06:02.741Z"
}
```

#### Admin requests

The following requests need a `avaliee_admin` role. To get this authorization, it is necessary issue an access token:

```bash
$ ADMIN_TOKEN=$(curl -s -H "Content-Type: application/x-www-form-urlencoded" -X POST http://localhost:8080/auth/realms/avaliee/protocol/openid-connect/token -d client_id=avaliee-webapp -d username=admin@avaliee.com.br -d password=admin -d grant_type=password | jq -r .access_token)
```

Then the access token stored into the `$ADMIN_TOKEN` variable can be passed to the requests.

##### Listing leads
```bash
$ curl -v -H "Authorization: Bearer $ADMIN_TOKEN" http://localhost:8000/api/v1/leads | jq
```
Expected response:
```
< HTTP/1.1 200 OK
< X-Powered-By: Express
< Content-Type: application/json; charset=utf-8
< Content-Length: 247
< ETag: W/"f7-F6zHPNFq6pEgWOBip5/gFn7mFWM"
< Date: Thu, 05 Mar 2020 02:11:33 GMT
< Connection: keep-alive
< 
{ [247 bytes data]
100   247  100   247    0     0  41166      0 --:--:-- --:--:-- --:--:-- 41166
* Connection #0 to host localhost left intact
[
  {
    "id": "740b6777-bc0d-4ee3-9032-694d792a91b9",
    "customerName": "Foo Bar",
    "customerEmail": "foo@bar.email",
    "customerPhone": "199901876303",
    "searchResult": {},
    "deleted": false,
    "createdAt": "2020-03-05T02:05:52.636Z",
    "updatedAt": "2020-03-05T02:05:52.636Z"
  }
]
```

##### Fetching a specific lead
```bash
$ curl -v -H "Authorization: Bearer $ADMIN_TOKEN" http://localhost:8000/api/v1/leads/740b6777-bc0d-4ee3-9032-694d792a91b9 | jq
```
Expected response:
```
< HTTP/1.1 200 OK
< X-Powered-By: Express
< Content-Type: application/json; charset=utf-8
< Content-Length: 245
< ETag: W/"f5-NK3FquKPItRdNiFeXWh2veb0xow"
< Date: Thu, 05 Mar 2020 02:07:17 GMT
< Connection: keep-alive
< 
{ [245 bytes data]
100   245  100   245    0     0  35000      0 --:--:-- --:--:-- --:--:-- 35000
* Connection #0 to host localhost left intact
{
  "id": "740b6777-bc0d-4ee3-9032-694d792a91b9",
  "customerName": "Foo Bar",
  "customerEmail": "foo@bar.email",
  "customerPhone": "199901876303",
  "searchResult": {},
  "deleted": false,
  "createdAt": "2020-03-05T02:06:02.741Z",
  "updatedAt": "2020-03-05T02:06:02.741Z"
}
```

##### Deleting a specific lead
```bash
$ curl -v -H "Authorization: Bearer $ADMIN_TOKEN" -X DELETE http://localhost:8000/api/v1/leads/740b6777-bc0d-4ee3-9032-694d792a91b9 | jq
```
Expected response:
```
< HTTP/1.1 204 No Content
< X-Powered-By: Express
< Date: Thu, 05 Mar 2020 02:08:01 GMT
< Connection: keep-alive
< 
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
* Connection #0 to host localhost left intact
```
